import 'dart:convert' as convert;
import 'dart:developer';

import 'package:http/http.dart' as http;

import 'utils.dart';
import 'exceptions.dart';
import 'models/errors.dart';


class BackendHttpClient extends http.BaseClient {
  final http.Client client = http.Client();
  final String domain = 'https://finance-app-backend.herokuapp.com';

  Future<http.StreamedResponse> send(http.BaseRequest request) async {
    Map<String, String> authData = await getAuthData();
    String authToken = authData['apiKey'];
    if (authToken != '') {
      request.headers['Authorization'] = 'Token ' + authToken;
    }
    request.headers['Content-Type'] = 'application/json; charset=UTF-8';
    return client.send(request);
  }

  Future<Map> registrationUser(Map<String, String> data) async {
    http.Response response = await http.post(
      domain + '/user/registration/',
      body: convert.jsonEncode(data)
    );
    return Map<String, String>.from(
        ResponseHandler(response).handleResponse()
    );
  }

  Future<Map<String, String>> login(Map<String, String> data) async {
    http.Response response = await http.post(
        domain + '/user/login/',
        body: convert.jsonEncode(data)
    );
    return Map<String, String>.from(ResponseHandler(response).handleResponse());
  }

  Future resetPasswordCreatePin(Map<String, String> data) async {
    http.Response response = await http.post(
        domain + '/user/reset_password/generate_pin/',
        body: convert.jsonEncode(data)
    );
    ResponseHandler(response).handleResponse();
  }

  Future resetPassword(Map<String, String> data) async {
    http.Response response = await http.post(
        domain + '/user/reset_password/',
        body: convert.jsonEncode(data)
    );
    return Map<String, String>.from(ResponseHandler(response).handleResponse());
  }

  Future logout() async {
    http.Response response = await http.post(
        domain + '/user/logout/'
    );
    ResponseHandler(response).handleResponse();
  }

  Future operationsList(
      int limit,
      int offset,
      List<int> categories,
      DateTime start,
      DateTime end
      ) async {
    http.Response response = await http.get(
        domain + '/operations/'
    );
    return Map<String, String>.from(ResponseHandler(response).handleResponse());
  }
}

class ResponseHandler {
  final http.Response response;

  ResponseHandler(this.response);

  handleResponse() {
    dynamic body;
    try {
      if (response.body.isNotEmpty){
        body = convert.jsonDecode(convert.utf8.decode(response.bodyBytes));
      }

    } catch(e, stackTrace) {
      log(e.toString());
      log(stackTrace.toString());
      throw DataRepositoryException(
          Errors(generalErrors: ['Ошибка передачи данных'])
      );
    }
    if (response.statusCode == 200 || response.statusCode == 201) {
      return body;
    } else {
      throw DataRepositoryException(Errors.fromJson(body));
    }
  }
}