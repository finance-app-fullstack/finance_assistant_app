import 'package:meta/meta.dart';


@immutable
class Operation {
  final int id;
  final int amount;
  final String name;
  final String category;
  final int categoryId;
  final DateTime created;

  Operation(this.id, this.name, this.amount, this.category,
      String created, {this.categoryId}) :
        created=DateTime.parse(created);
}

@immutable
class OperationCategory {
  final int id;
  final String name;

  OperationCategory(this.id, this.name);

}

@immutable
class OperationFilterCategory extends OperationCategory {
  final bool checked;

  OperationFilterCategory(int id, String name, bool checked) :
        this.checked = checked,
        super(id, name);
}

@immutable
class OperationCategoryFullInfo extends OperationCategory {
  final int operationsCount;

  OperationCategoryFullInfo(int id, String name, int operationsCount) :
        this.operationsCount = operationsCount,
        super(id, name);
}

@immutable
class OperationCategoryStatInfo {
  final String name;
  final int sum;

  OperationCategoryStatInfo(this.name, this.sum);
}

@immutable
class OperationFilter {
  final String searchWord;
  final List<OperationFilterCategory> categories;
  final DateTime start;
  final DateTime end;

  OperationFilter(this.searchWord, this.categories, this.start, this.end);

  OperationFilter.init(this.categories,
      {
        this.searchWord: '',
        this.start,
        this.end,
      });

  factory OperationFilter.initFormWithData(
      word,
      Map<int, bool> changedCategories,
      List<OperationFilterCategory> categories,
      DateTime start,
      DateTime end
      ){

      if (changedCategories.isEmpty) {
        List <OperationFilterCategory> changedFormCategories = [];
        categories.forEach((category) {
          bool checked;
          if (changedCategories.containsKey(category.id)) {
            checked = changedCategories[category.id];
          } else {
            checked = category.checked;
          }
          changedFormCategories.add(OperationFilterCategory(
              category.id,
              category.name,
              checked
          ));
        });
        return OperationFilter(word, changedFormCategories, start, end);
      } else{
        return OperationFilter(word, categories, start, end);
      }

  }
}