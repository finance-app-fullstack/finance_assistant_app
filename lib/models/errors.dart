
class Errors {
  final List<String> generalErrors;
  final Map<String, List<String>> fieldLevelErrors;

  Errors({generalErrors, fieldLevelErrors}) :
        generalErrors = generalErrors != null ? generalErrors : [],
        fieldLevelErrors = fieldLevelErrors != null ? fieldLevelErrors: {};

  String get generalErrorsAsString {
    if (generalErrors.length > 0) {
      String errors = '';
      generalErrors.forEach((element) {
        errors += element + '\n';
      });
      errors = errors.substring(0, errors.length - 1);
      return errors;
    } else {
      return '';
    }
  }

  Map<String, String> get fieldLevelAsString {
    Map<String, String> errors = {};
    fieldLevelErrors.forEach((key, value) {
      errors[key] = value.join(', ');
    });
    return errors;
  }

  factory Errors.fromJson(Map<dynamic, dynamic> json){
    if (json.containsKey('error_details')) {
      Map<String, dynamic> detailsBody = json['error_details'];
      List<String> general;
      Map<String, List<String>> fieldLevelErrors;

      if (detailsBody.containsKey('__root__')) {
        general = List<String>.from(detailsBody['__root__']);
        detailsBody.remove('__root__');
      }

      if (detailsBody.isNotEmpty) {
        fieldLevelErrors = {};
        detailsBody.forEach((key, value) {
          fieldLevelErrors[key] = List<String>.from(value);
        });
      }

      return Errors(
          generalErrors: general,
          fieldLevelErrors: fieldLevelErrors
      );
    } else {
      return Errors(generalErrors: <String>[json['error_detail']]);
    }
  }

  @override
  String toString() {
    return """
     General errors: ${generalErrors.toString()};
     Fields errors: ${fieldLevelErrors.toString()}
     """;
  }
}