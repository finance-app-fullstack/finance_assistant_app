import 'dart:developer';

import 'package:flutter/widgets.dart';

import '../repository.dart';
import '../models/operation.dart';


class CategoriesListViewModel extends ChangeNotifier {
  bool loading = true;
  List<OperationCategory> categories;
  OperationsRepository repository;

  CategoriesListViewModel(this.repository);

  Future loadCategories() async {
    loading = true;
    notifyListeners();
    log('loadCats');
    categories = await repository.getCategories();

    loading = false;
    notifyListeners();
  }
}