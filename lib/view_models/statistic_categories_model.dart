import 'dart:developer';

import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

import '../repository.dart';
import '../models/operation.dart';


class StatisticCategoriesViewModel extends ChangeNotifier {
  bool loading = true;
  DateTime start;
  DateTime end;
  List<OperationCategoryStatInfo> categoriesStat;
  OperationsRepository repository;

  StatisticCategoriesViewModel(this.repository);

  Future loadStat({DateTime start, DateTime end}) async {

    if((start == null || end == null) || (end.isBefore(start))) {
      this.end = DateTime.now();
      this.start = this.end.subtract(Duration(days: 7));
    } else {
      this.start = start;
      this.end = end;
    }

    loading = true;
    notifyListeners();

    categoriesStat = await repository.getStatisticInfo(this.start, this.end);

    loading = false;
    notifyListeners();
  }

  Map<String, double> get dataForChart {
    Map<String, double> result = {};

    categoriesStat.forEach((element) {
      result[element.name] = element.sum.toDouble();
    });

    return result;
  }

  String get stringDateRange {
    if (start == null && end == null) {
      return "";
    }
    DateFormat dayFormatter = DateFormat('d MMMM y', 'ru_RU');
    return "${dayFormatter.format(start)} - ${dayFormatter.format(end)}";
  }
}