import 'package:financeappprovider/models/errors.dart';
import 'package:flutter/cupertino.dart';

import '../exceptions.dart';
import '../repository.dart';


class LoginFormViewModel extends ChangeNotifier {
  bool loading;
  Errors errors;
  OperationsRepository repository;

  LoginFormViewModel(this.repository, {this.loading: false});

  Future<bool> login(String email, String password) async {
    loading = true;
    notifyListeners();

    try {
      return await repository.login(email, password);

    } on DataRepositoryException catch (e) {
      loading = false;
      errors = e.errors;
      notifyListeners();
      return false;

    } catch (e) {
      errors = Errors(generalErrors: 'Неизвестная ошибка, попробуйте еще раз.');
      loading = false;
      notifyListeners();
      return false;
    }
  }

}