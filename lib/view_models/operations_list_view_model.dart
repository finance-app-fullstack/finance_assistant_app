import 'dart:developer';

import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

import '../repository.dart';
import '../models/operation.dart';

class OperationsListViewModel extends ChangeNotifier {
  bool loading = false;
  bool fullLoad = false;
  num currentPage = 1;
  List operations = [];
  OperationsRepository repository;

  // Поисковые поля
  List<int> categories;
  String dateRangeVariant;
  DateTime start;
  DateTime end;

  OperationsListViewModel(this.repository){
    loadOperations(initLoading: true);
  }

  Map get searchData => {'categories': categories, 'start': start,
    'end': end, 'dateRangeVariant': dateRangeVariant};

  bool get isSearch => categories != null && start != null && end != null;

  Future loadOperations({initLoading: false}) async {
    if (initLoading) {
      loading = true;
      fullLoad = false;
      operations.clear();
      currentPage = 1;
      notifyListeners();
    }

    List<Operation> newOperations = await repository.getOperations(
      currentPage,
      categories: categories,
      startDate: start,
      endDate: end
    );
    log(newOperations.toString());
    if (newOperations.isNotEmpty){
      mergeOperations(newOperations);
      currentPage += 1;
    } else {
      fullLoad = true;
    }


    if (initLoading) {
      loading = false;
    }

    notifyListeners();
  }

  void startSearch(
      List<int> categories,
      String dateRangeVariant,
      DateTime start,
      DateTime end
      )
  {
    this.categories = categories;
    this.dateRangeVariant = dateRangeVariant;

    if (start == null && end == null) {
      List<DateTime> range = getDateTimeRangeByVariant(dateRangeVariant);
      this.start = range[0];
      this.end = range[1];
    } else {
      this.start = start;
      this.end = end;
    }

    loadOperations(initLoading: true);
  }

  void endSearch(){
    this.categories = null;
    this.start = null;
    this.end = null;
    this.dateRangeVariant = null;

    loadOperations(initLoading: true);
  }

  void mergeOperations(List<Operation> newOperations){
    if(newOperations.isNotEmpty) {
      DateTime currentDate;
      DateFormat dayFormatter = DateFormat('d MMMM', 'ru_RU');
      /*
      * Если состояние не имеет операций, первый элемент должен быть датой,
      * так же необходимо установить текущую дату, для дальнейшей работы
      */
      if (operations.isEmpty) {
        currentDate = newOperations[0].created;
        if (newOperations[0].created.difference(DateTime.now()).inDays == 0){
          operations.add("Сегодня");
        }else {
          operations.add(dayFormatter.format(newOperations[0].created));
        }
      } else{
        currentDate = operations.last.created;
      }

      for (Operation operation in newOperations) {
        if (operation.created.day != currentDate.day){
          currentDate = operation.created;
          operations.add(dayFormatter.format(currentDate));
        }
        operations.add(operation);
      }
    }
  }

  List<DateTime> getDateTimeRangeByVariant(String variant){
    switch (variant){
      case 'yesterday':
        DateTime now = DateTime.now();
        int dayAgo = now.subtract(Duration(days: 1)).day;
        DateTime start = DateTime(
          now.year,
          now.month,
          dayAgo,
        );
        DateTime end = DateTime(
            now.year,
            now.month,
            dayAgo,
            23,
            59,
            59
        );
        return [start, end];
      case 'today':
        DateTime now = DateTime.now();
        DateTime start = DateTime(
          now.year,
          now.month,
          now.day,
        );
        DateTime end = DateTime(
            now.year,
            now.month,
            now.day,
            23,
            59,
            59
        );
        return [start, end];
      case 'week':
        DateTime now = DateTime.now();
        int dayAgo = now.subtract(Duration(days: 7)).day;
        DateTime start = DateTime(
          now.year,
          now.month,
          dayAgo,
        );
        DateTime end = DateTime(
            now.year,
            now.month,
            dayAgo,
            23,
            59,
            59
        );
        return [start, end];
      default: {
        return [null, null];
      }
    }
  }
}