import 'package:financeappprovider/view_models/statistic_categories_model.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pie_chart/pie_chart.dart';

import '../../utils.dart';


class CategoriesPieChart extends StatelessWidget {
  final StatisticCategoriesViewModel viewModel;

  CategoriesPieChart(this.viewModel);

  Widget buildCategoriesTextStatWidgets({
    Map<String, double> categories,
    String dateHeader,
    BuildContext context,
  })
  {
    List<Widget> categoriesList = List<Widget>();
    NumberFormat numberFormatter = getAmountFormatter();

    categories.forEach((name, sum) {
      categoriesList.add(
        Container(
          width: double.infinity,
          margin: EdgeInsets.only(left: 15, bottom: 12),
          child: RichText(
            text: TextSpan(
              style: DefaultTextStyle.of(context).style,
              text: '$name: ',
              children: <TextSpan>[
                TextSpan(text: numberFormatter.format(sum),
                    style: TextStyle(fontWeight: FontWeight.bold)),
              ],
            ),
          ),
        ),
      );
    });
    categoriesList.insert(
      0,
      Container(
        width: double.infinity,
        margin: EdgeInsets.only(left: 15, bottom: 15),
        child: Text(
            '$dateHeader',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)
        ),
      ),
    );

    return Column(
        children: categoriesList,
    );
  }

  Widget build(BuildContext context){
    Map<String, double> chartData = viewModel.dataForChart;
    return Center(
      child:ListView(
        padding: EdgeInsets.only(top: 12, bottom: 5),
        children: <Widget>[
          Card(
            elevation: 4,
            child: Padding(
              padding: EdgeInsets.only(top: 10),
              child: buildCategoriesTextStatWidgets(
                categories: chartData,
                dateHeader: viewModel.stringDateRange,
                context: context
              ),
            ),
          ),
          SizedBox(height: 10),
          Card(
            elevation: 4,
            child: PieChart(dataMap: chartData),
          ),
        ],
      ),
    );
  }
}