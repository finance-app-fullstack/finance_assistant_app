import 'package:financeappprovider/repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../models/operation.dart';
import 'delete_alert.dart';
import '../../screens/category_form.dart';


class CategoryDetail extends StatelessWidget {
  final OperationCategoryFullInfo category;
  final OperationsRepository operationsRepository;

  const CategoryDetail({
    Key key,
    @required this.category,
    @required this.operationsRepository
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: double.infinity,
              margin: EdgeInsets.fromLTRB(16, 16, 0, 5),
              child: Text(
                category.name,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.fromLTRB(16, 5, 0, 10),
              child: RichText(
                text: TextSpan(
                  style: DefaultTextStyle.of(context).style,
                  text: 'Число операций: ',
                  children: <TextSpan>[
                    TextSpan(
                        text: category.operationsCount.toString(),
                        style: TextStyle(fontWeight: FontWeight.bold)
                    ),
                  ],
                ),
              ),
            ),
            ButtonBar(
              children: <Widget>[
                FlatButton(
                  textColor: Colors.redAccent,
                  child: Text('Удалить'),
                  onPressed: () {
                    showDialog(
                      context: context,
                      barrierDismissible: false,
                      child: RemoveCategoryDialog(
                        categoryId: category.id,
                        operationsRepository: operationsRepository,
                      )
                    );
                  },
                ),
                FlatButton(
                  child: Text('Редактировать'),
                  onPressed: () {
                    Navigator.push(context,
                      MaterialPageRoute(
                          builder: (context) => CategoryFormScreen(
                            repository: operationsRepository,
                            category: category,
                          )
                      ),
                    );
                  },
                ),
              ],
            ),
          ],
      ),
    );
  }
}