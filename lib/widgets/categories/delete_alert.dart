import 'package:financeappprovider/repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';


class RemoveCategoryDialog extends StatefulWidget {
  final OperationsRepository operationsRepository;
  final int categoryId;

  const RemoveCategoryDialog({
    Key key,
    @required this.operationsRepository,
    @required this.categoryId
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return RemoveCategoryDialogState();
  }

}

class RemoveCategoryDialogState extends State<RemoveCategoryDialog> {
  bool loading;
  String text;

  @override
  void initState() {
    loading = false;
    text = 'Категория и операции будут удалены навсегда, продолжить?';
    super.initState();
  }

  void _removeProcess() {
    setState(() => loading = true);
    widget.operationsRepository.deleteCategory(widget.categoryId)
        .then((value) {
          Navigator.of(context).pop();
          Navigator.of(context).pop(true);
        })
        .catchError((onError) {
          setState(() {
            text = 'Ошибка при удалении операции, попробуйте позже';
            loading =false;
          });
        });
  }

  void _cancel() => Navigator.of(context).pop();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Удаление категории'),
      content: loading ?
      Center(widthFactor: 2, heightFactor: 2,
          child: CircularProgressIndicator())
          : Text(text),
      actions: [
        FlatButton(
          textColor: Colors.redAccent,
          child: Text('Удалить'),
          onPressed: loading ? null : _removeProcess,
        ),
        FlatButton(
          child: Text('Отмена'),
          onPressed: loading ? null : _cancel
        ),
      ],
    );
  }

}