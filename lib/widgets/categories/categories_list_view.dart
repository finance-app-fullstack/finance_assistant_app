import 'package:financeappprovider/view_models/operations_list_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import '../../view_models/categories_list_view_model.dart';
import '../../models/operation.dart';
import '../../screens/category_detail_screen.dart';


class CategoriesListView extends StatelessWidget {
  final CategoriesListViewModel viewModel;
  final Function reloadCategories;

  const CategoriesListView({
    Key key,
    this.viewModel,
    this.reloadCategories
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          OperationCategory item = viewModel.categories[index];
          return ListTile(
            title: Center(
                child: Text(
                    item.name,
                    style: TextStyle(
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold
                    )
                )
            ),
            onTap: () async {
              final result = await Navigator.push(context,
                MaterialPageRoute(
                    builder: (context) => CategoryDetailScreen(
                      categoryId: item.id,
                      repository: viewModel.repository,
                    )
                ),
              );

              if (result != null) {
                reloadCategories();
                Provider.of<OperationsListViewModel>(context, listen: false)
                    .loadOperations(initLoading: true);
              }
            }
          );
        },
        itemCount: viewModel.categories.length
    );
  }
}