import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DateHeaderWidget extends StatelessWidget {
  final String dateHeaderText;

  const DateHeaderWidget({Key key, @required this.dateHeaderText}) :
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
          dateHeaderText,
          style: TextStyle(fontSize: 19.0, fontWeight: FontWeight.bold)
      ),
    );
  }
}