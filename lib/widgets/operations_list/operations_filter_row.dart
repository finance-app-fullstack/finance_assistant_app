import 'package:flutter/material.dart';


class OperationFilterFormRow extends StatelessWidget {
  final String label;
  final Widget child;

  OperationFilterFormRow(this.label, this.child);

  Widget build(BuildContext context){
    return Center(
      child: Card(
        elevation: 4,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              title: Text(
                label,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(16, 0, 16, 20),
              child: child,
            )
          ],
        ),
      ),
    );
  }
}