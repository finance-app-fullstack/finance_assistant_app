import 'package:financeappprovider/repository.dart';
import 'package:financeappprovider/screens/operation_detail_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

import '../../models/operation.dart';


class OperationWidget extends StatelessWidget {
  final Operation operation;
  final OperationsRepository repository;
  final Function reload;
  final NumberFormat formatter;

  const OperationWidget({
    Key key,
    @required this.operation,
    @required this.repository,
    @required this.reload,
    @required this.formatter
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
          operation.name,
          style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.bold)
      ),
      subtitle: Text(operation.category),
      onTap: () async {
        final result = await Navigator.push(context,
          MaterialPageRoute(
              builder: (context) => OperationDetailScreen(
                operationId: operation.id,
                repository: repository,
              )
          ),
        );
        if (result != null) {
          reload(initLoading: true);
        }
      },
      trailing: Text(
          formatter.format(operation.amount),
          style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.bold)
      ),
    );
  }
}