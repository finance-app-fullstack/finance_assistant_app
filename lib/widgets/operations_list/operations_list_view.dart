import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

import 'operations_list_date_header.dart';
import 'operations_list_item.dart';
import 'operations_list_loader.dart';
import '../../view_models/operations_list_view_model.dart';
import '../../models/operation.dart';
import '../../utils.dart';


class OperationsListView extends StatelessWidget {
  final OperationsListViewModel viewModel;

  const OperationsListView({Key key, this.viewModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    NumberFormat numberFormatter = getAmountFormatter();
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        if(index >= viewModel.operations.length) {
          viewModel.loadOperations();
          return BottomLoader();
        }else{
          if (viewModel.operations[index] is Operation){
            return OperationWidget(
              operation: viewModel.operations[index],
              repository: viewModel.repository,
              reload: viewModel.loadOperations,
              formatter: numberFormatter
            );
          }
          else{
            return DateHeaderWidget(
                dateHeaderText: viewModel.operations[index]
            );
          }

        }
      },
      itemCount: viewModel.fullLoad ? viewModel.operations.length
          : viewModel.operations.length + 1
    );
  }
}
