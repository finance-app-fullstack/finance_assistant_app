import 'dart:developer';

import 'package:chips_choice/chips_choice.dart';
import 'package:flutter/material.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;

import '../../repository.dart';
import 'operations_filter_row.dart';


class OperationFilterForm extends StatefulWidget {
  final Map searchData;
  final OperationsRepository repository;

  const OperationFilterForm({Key key, this.searchData, this.repository}) :
        super(key: key);

  @override
  State<StatefulWidget> createState() => OperationFilterFormRowsState();

}

class OperationFilterFormRowsState extends State<OperationFilterForm> {
  List<ChipsChoiceOption<dynamic>> categories;
  List<int> selectCategories;

  ChipsChoiceOption customDateRangeChoice;

  List<ChipsChoiceOption<dynamic>> dateVariants;

  String dateSelect;
  DateTime startDate;
  DateTime endDate;

  bool loading;


  @override
  void initState() {
    customDateRangeChoice =
        ChipsChoiceOption(value: 'custom', label: 'Свой вариант');

    selectCategories = [];
    categories = [];

    if (widget.searchData['categories'] != null) {
      widget.searchData['categories'].forEach((element) {
        selectCategories.add(element);
      });
    }

    dateVariants = [];
    dateVariants.add(ChipsChoiceOption(value: 'all', label: 'За все время'));
    dateVariants.add(ChipsChoiceOption(value: 'yesterday', label: 'Вчера'));
    dateVariants.add(ChipsChoiceOption(value: 'today', label: 'Сегодня'));
    dateVariants.add(ChipsChoiceOption(value: 'week', label: 'За текущую '
        'неделю'));
    dateVariants.add(ChipsChoiceOption(value: 'month', label: 'За текущий '
        'месяц'));
    log(widget.searchData.toString());
    if (widget.searchData['dateRangeVariant'] != null) {
      dateSelect = widget.searchData['dateRangeVariant'];
      if (widget.searchData['dateRangeVariant'] == 'custom') {
        startDate = widget.searchData['start'];
        endDate = widget.searchData['end'];
        dateVariants.add(createCustomChoice(startDate, endDate));
      } else {
        dateVariants.add(customDateRangeChoice);
      }
    } else {
      dateSelect = 'all';
    }

    loading = true;
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_){
      widget.repository.getCategories()
          .then((value) {
            setState(() {
              value.forEach((element) {
                categories.add(ChipsChoiceOption(
                    value: element.id,
                    label: element.name
                ));
              });

              if (selectCategories.length == 0) {
                categories.forEach((categoryChip) {
                  selectCategories.add(categoryChip.value);
                });
              }

              loading =false;
            });
          })
          .catchError((onError) {
            setState(() {
              loading =false;
            });
          });
    });
  }

  Future<List<DateTime>> showDatePicker() async {
    DateTime now = DateTime.now();
    if (startDate == null && endDate == null) {
      startDate = now.subtract(Duration(days: 3));
      endDate = now;
    }
    return DateRagePicker.showDatePicker(
        context: context,
        initialFirstDate: startDate,
        initialLastDate: endDate,
        firstDate: DateTime(2000),
        lastDate: DateTime(now.year, now.month, now.day, 23, 59, 59)
    );
  }

  @override
  Widget build(BuildContext context) {
    return loading ?
      Center(child: CircularProgressIndicator()) :
      SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 5.0),
          child: Column(
            children: [
              OperationFilterFormRow(
                  'По категориям',
                  ChipsChoice.multiple(
                    padding: EdgeInsets.zero,
                    value: selectCategories,
                    isWrapped: true,
                    options: categories,
                    onChanged: (val) => setState((){
                      selectCategories = [];
                      val.forEach((element) {
                        selectCategories.add(element);
                      });

                    }),
                    itemConfig: ChipsChoiceItemConfig(
                        spacing: 20.0,
                        runSpacing: 0.0
                    ),
                  )
              ),
              OperationFilterFormRow(
                  'По дате',
                  ChipsChoice.single(
                    padding: EdgeInsets.zero,
                    value: dateSelect,
                    isWrapped: true,
                    options: dateVariants,
                    onChanged: (val) async {
                      if (val == 'custom') {
                        final List<DateTime> picked = await showDatePicker();
                        if (picked != null){
                          setState(() {
                            dateVariants.removeLast();
                            dateVariants.add(
                              createCustomChoice(picked[0], picked[1])
                            );
                            dateSelect = 'custom';
                            startDate = picked[0];
                            endDate = picked[1];
                          });
                        }
                      } else {
                        if (startDate == null && endDate == null){
                          setState(() => dateSelect = val);
                        } else {
                          setState(() {
                            dateVariants.removeLast();
                            dateVariants.add(customDateRangeChoice);
                            dateSelect = val;
                            startDate = null;
                            endDate = null;
                          });
                        }
                      }
                    },
                    itemConfig: const ChipsChoiceItemConfig(
                        spacing: 20.0,
                        runSpacing: 0.0
                    ),
                  )
              ),
              Container(
                margin: EdgeInsets.fromLTRB(4, 6, 4, 5),
                //padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                width: double.infinity, // match_parent
                height: 50,
                child: RaisedButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  elevation: 4,
                  onPressed: () {
                    Navigator.pop(context, {
                      'categories': selectCategories,
                      'start': startDate,
                      'end': endDate,
                      'dateRangeVariant': dateSelect,
                    });
                  },
                  child: Text("Показать результаты"),
                ),
              ),
            ],
          ),
        )
    );
  }

  ChipsChoiceOption createCustomChoice(DateTime start, DateTime end){
    return ChipsChoiceOption(
        value: 'custom',
        label: '${start.day}.${start.month}.'
            '${start.year} - '
            '${end.day}.${end.month}.'
            '${end.year}'
    );
  }
}