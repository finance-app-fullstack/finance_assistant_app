import 'package:financeappprovider/repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';


class RemoveOperationDialog extends StatefulWidget {
  final OperationsRepository operationsRepository;
  final int operationId;

  const RemoveOperationDialog({
    Key key,
    @required this.operationsRepository,
    @required this.operationId
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return RemoveOperationDialogState();
  }

}

class RemoveOperationDialogState extends State<RemoveOperationDialog> {
  bool loading;
  String text;

  @override
  void initState() {
    loading = false;
    text = 'Операция будет удалена совсем, продолжить?';
    super.initState();
  }

  void _removeProcess() {
    setState(() => loading = true);
    widget.operationsRepository.deleteOperation(widget.operationId)
        .then((value) {
          Navigator.of(context).pop();
          Navigator.of(context).pop(true);
        })
        .catchError((onError) {
          setState(() {
            text = 'Ошибка при удалении операции, попробуйте позже';
            loading =false;
          });
        });
  }

  void _cancel() => Navigator.of(context).pop();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Удаление операции'),
      content: loading ?
      Center(widthFactor: 2, heightFactor: 2, child: CircularProgressIndicator())
          : Text(text),
      actions: [
        FlatButton(
          textColor: Colors.redAccent,
          child: Text('Удалить'),
          onPressed: loading ? null : _removeProcess,
        ),
        FlatButton(
          child: Text('Отмена'),
          onPressed: loading ? null : _cancel
        ),
      ],
    );
  }

}