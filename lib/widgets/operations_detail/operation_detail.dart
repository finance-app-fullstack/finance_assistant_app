import 'package:financeappprovider/repository.dart';
import 'package:financeappprovider/screens/operation_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

import '../../models/operation.dart';
import '../../utils.dart';
import 'delete_alert.dart';


class OperationDetail extends StatelessWidget {
  final Operation operation;
  final OperationsRepository operationsRepository;

  const OperationDetail({
    Key key,
    @required this.operation,
    @required this.operationsRepository
  }) : super(key: key);

  String _formatCreatedDate(DateTime date){
    DateFormat dateFormatter = DateFormat('d MMM y HH:m:ss', 'ru_RU');
    return dateFormatter.format(date);
  }

  @override
  Widget build(BuildContext context) {
    NumberFormat numberFormatter = getAmountFormatter();
    return Card(
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: double.infinity,
              margin: EdgeInsets.fromLTRB(16, 16, 0, 5),
              child: Text(
                operation.name,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.fromLTRB(16, 20, 0, 10),
              child: RichText(
                text: TextSpan(
                  style: DefaultTextStyle.of(context).style,
                  text: 'Сумма: ',
                  children: <TextSpan>[
                    TextSpan(text: numberFormatter.format(operation.amount),
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.fromLTRB(16, 5, 0, 10),
              child: RichText(
                text: TextSpan(
                  style: DefaultTextStyle.of(context).style,
                  text: 'Категория: ',
                  children: <TextSpan>[
                    TextSpan(
                        text: operation.category,
                        style: TextStyle(fontWeight: FontWeight.bold)
                    ),
                  ],
                ),
              ),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.fromLTRB(16, 5, 0, 10),
              child: RichText(
                text: TextSpan(
                  style: DefaultTextStyle.of(context).style,
                  text: 'Дата создания: ',
                  children: <TextSpan>[
                    TextSpan(
                        text: _formatCreatedDate(operation.created),
                        style: TextStyle(fontWeight: FontWeight.bold)
                    ),
                  ],
                ),
              ),
            ),
            ButtonBar(
              children: <Widget>[
                FlatButton(
                  textColor: Colors.redAccent,
                  child: Text('Удалить'),
                  onPressed: () {
                    showDialog(
                        barrierDismissible: false,
                        context: context,
                        child: RemoveOperationDialog(
                          operationId: operation.id,
                          operationsRepository: operationsRepository,
                        )
                    );
                  },
                ),
                FlatButton(
                  child: Text('Редактировать'),
                  onPressed: () {
                    Navigator.push(context,
                      MaterialPageRoute(
                          builder: (context) => OperationFormScreen(
                            repository: operationsRepository,
                            operation: operation,
                          )
                      ),
                    );
                  },
                ),
              ],
            ),
          ],
      ),
    );
  }
}