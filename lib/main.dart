import 'dart:developer';

import 'package:financeappprovider/view_models/statistic_categories_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

import 'repository.dart';
import 'screens/auth_screens/auth_base_widget.dart';
import 'screens/home_screen.dart';
import 'utils.dart';
import 'view_models/operations_list_view_model.dart';
import 'view_models/categories_list_view_model.dart';

void main() async {
  String initialRoute;

  WidgetsFlutterBinding.ensureInitialized();

  Map<String, String> authData = await getAuthData();
  if (authData['apiKey'] == null) {
    initialRoute = 'login';
  } else {
    initialRoute = 'home';
  }
  runApp(FinanceApp(initialRoute: initialRoute));
}

class FinanceApp extends StatelessWidget {
  final String initialRoute;

  FinanceApp({Key key, this.initialRoute}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Финансовый помощник',
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('ru'),
          const Locale.fromSubtags(languageCode: 'ru'),
        ],
        initialRoute: initialRoute,
        routes: {
          'home': (context) {
            OperationsRepository repository = OperationsRepository();
            return MultiProvider(
                providers: [
                  ChangeNotifierProvider<OperationsListViewModel>(create: (_) =>
                      OperationsListViewModel(repository)),
                  ChangeNotifierProvider<CategoriesListViewModel>(create: (_) =>
                      CategoriesListViewModel(repository)),
                  ChangeNotifierProvider<StatisticCategoriesViewModel>(
                      create: (_) => StatisticCategoriesViewModel(repository)
                  ),
                ],
                child: HomeScreen(repository: repository)
            );
          },
          'login': (context) {
            return AuthScreen(title: 'Вход', formId: 'login');
          },
          'registration': (context) {
            return AuthScreen(title: 'Регистрация', formId: 'registration');
          },
          'reset_password': (context) {
            return AuthScreen(title: 'Восстановление пароля',
                formId: 'reset_password');
          },
          'reset_password_confirm': (context) {
            return AuthScreen(title: 'Восстановление пароля',
                formId: 'reset_password_confirm');
          },
        },
    );
  }
}

