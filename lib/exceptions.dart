import 'models/errors.dart';


class DataRepositoryException implements Exception {
  final Errors errors;

  DataRepositoryException(this.errors);
}

class AuthException extends DataRepositoryException {

  AuthException() : super(Errors(generalErrors: ['Ошибка аавторизации']));

}