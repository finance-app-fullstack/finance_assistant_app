import 'dart:developer';

import 'package:financeappprovider/repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../utils.dart';
import 'change_password.dart';


class ProfileScreen extends StatelessWidget {
  final OperationsRepository repository;

  const ProfileScreen({Key key, this.repository}) :
        super(key: key);

  String getUserEmail(Map data) {
    String email = data['email'];
    if (email == null){
      email = '';
    }
    return email;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getAuthData(),
        builder: (
            BuildContext context,
            AsyncSnapshot<Map<String, String>>snapshot)
        {
          if (snapshot.hasData) {
            return ListView(
                padding: EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 50.0),
                children: <Widget>[
                  Text(
                    'Email: ' + getUserEmail(snapshot.data),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 22,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 20.0),
                  RaisedButton(
                    color: Colors.lightBlueAccent,
                    padding: EdgeInsets.symmetric(vertical: 15.0),
                    child: Text(
                      'Сменить пароль',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      ),
                    ),
                    onPressed: () async {
                      final result = await Navigator.push(context,
                        MaterialPageRoute(
                            builder: (context) => ChangePasswordScreen(
                              repository: repository,
                            )
                        ),
                      );
                      if (result) {
                        showSuccessSnackBar('Пароль успешно изменен.', context);
                      }
                    },
                  ),
                  SizedBox(height: 20.0),
                  RaisedButton(
                    color: Colors.redAccent,
                    padding: EdgeInsets.symmetric(vertical: 15.0),
                    child: Text(
                      'Выйти',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      ),
                    ),
                    onPressed: () {
                      showDialog(
                          barrierDismissible: false,
                          context: context,
                          child: AlertDialog(
                            title: Text('Выйти?'),
                            actions: [
                              FlatButton(
                                textColor: Colors.redAccent,
                                child: Text('Выйти'),
                                onPressed: () {
                                  repository.logout();
                                  Navigator.of(context).popAndPushNamed(
                                      'login');
                                },
                              ),
                              FlatButton(
                                  child: Text('Отмена'),
                                  onPressed: () => Navigator.of(context).pop()
                              ),
                            ],
                          )
                      );
                    },
                  ),
                ]
            );
          } else if (snapshot.hasError) {
            return Text('Произошла ошибка');
          } else {
            return CircularProgressIndicator();
          }
    });
  }
}
