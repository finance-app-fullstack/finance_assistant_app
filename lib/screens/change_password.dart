import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../exceptions.dart';
import '../repository.dart';
import '../utils.dart';
import '../models/errors.dart';

class ChangePasswordScreen extends StatefulWidget {
  final OperationsRepository repository;

  const ChangePasswordScreen({Key key, this.repository}) : super(key: key);

  @override
  State<StatefulWidget> createState() => ChangePasswordScreenState();

}

class ChangePasswordScreenState extends State<ChangePasswordScreen> {
  TextEditingController passwordController;
  TextEditingController passwordConfirmController;
  Errors errors;
  bool loading;

  @override
  void initState() {
    passwordController = TextEditingController();
    passwordConfirmController = TextEditingController();
    errors = Errors();
    loading = false;
    super.initState();
  }

  Future changePassword() async {
    setState(() => loading = true);

    try {
      await widget.repository.changePassword(
          passwordController.text,
          passwordConfirmController.text
      );
      Navigator.of(context).pop(true);
    } on DataRepositoryException catch(e) {
      log(e.errors.toString());
      setState(() {
        loading = false;
        errors = e.errors;
      });
      if (errors.generalErrorsAsString.length > 0) {
        showErrorSnackBar(errors.generalErrorsAsString, context);
      }
    }
  }

  Widget buildPasswordChangeForm(BuildContext context) {
    return Form(
      child: ListView(
        padding: EdgeInsets.fromLTRB(24.0, 50.0, 24.0, 50.0),
        children: <Widget>[
          TextFormField(
            obscureText: true,
            keyboardType: TextInputType.number,
            controller: passwordController,
            decoration: InputDecoration(
                hintText: 'Новый пароль',
                errorText: errors.fieldLevelAsString['password']
            ),
          ),
          SizedBox(height: 30.0),
          TextFormField(
            obscureText: true,
            keyboardType: TextInputType.number,
            controller: passwordConfirmController,
            decoration: InputDecoration(
                hintText: 'Подтверждение пароля',
                errorText: errors.fieldLevelAsString['passwordConfirm']
            ),
          ),
          SizedBox(height: 30.0),
          RaisedButton(
              color: Colors.lightBlueAccent,
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                  'Сменить пароль',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0
                  )
              ),
              onPressed: () => changePassword()
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Информация о категории'),
      ),
      body: loading ? Center(child: CircularProgressIndicator()) :
      buildPasswordChangeForm(context),
    );
  }

  @override
  void dispose() {
    passwordController.dispose();
    passwordConfirmController.dispose();
    super.dispose();
  }
}