import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../exceptions.dart';
import '../../repository.dart';
import '../../models/errors.dart';
import '../../utils.dart';

class RegistrationScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => RegistrationScreenState();

}

class RegistrationScreenState extends State<RegistrationScreen> {
  TextEditingController emailController;
  TextEditingController passwordController;
  TextEditingController passwordConfirmController;
  Errors errors;
  bool loading;

  @override
  void initState() {
    emailController = TextEditingController();
    passwordController = TextEditingController();
    passwordConfirmController = TextEditingController();
    errors = Errors();
    loading = false;
    super.initState();
  }

  Future registration() async {
    setState(() => loading = true);

    OperationsRepository apiRepo = OperationsRepository();
    try {
      await apiRepo.registration(
        emailController.text,
        passwordController.text,
        passwordConfirmController.text
      );

      Navigator.of(context).pushReplacementNamed('home');
    } on DataRepositoryException catch(e) {
      setState(() {
        loading = false;
        errors = e.errors;
      });
      if (errors.generalErrorsAsString.length > 0) {
        showErrorSnackBar(errors.generalErrorsAsString, context);
      }

    } catch (e) {
      log(e.toString());
      setState(() => loading = false);
      showErrorSnackBar('При авторизации произошла ошибка', context);
    }

  }


  @override
  Widget build(BuildContext context) {
    return loading ?
    Center(child: CircularProgressIndicator()) :
    Form(
      child: ListView(
        padding: EdgeInsets.fromLTRB(24.0, 50.0, 24.0, 50.0),
        children: <Widget>[
          TextFormField(
            keyboardType: TextInputType.emailAddress,
            controller: emailController,
            decoration: InputDecoration(
                hintText: 'Email',
                errorText: errors.fieldLevelAsString['email']
            ),
          ),
          SizedBox(height: 16.0),
          TextFormField(
            obscureText: true,
            controller: passwordController,
            decoration: InputDecoration(
                hintText: 'Пароль',
                errorText: errors.fieldLevelAsString['password']
            ),
          ),
          SizedBox(height: 16.0),
          TextFormField(
            obscureText: true,
            controller: passwordConfirmController,
            decoration: InputDecoration(
                hintText: 'Повторите пароль',
                errorText: errors.fieldLevelAsString['password_confirm']
            ),
          ),
          SizedBox(height: 30.0),
          RaisedButton(
              color: Colors.lightBlueAccent,
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                  'Зарегистрироваться',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0
                  )
              ),
              onPressed: () => registration()
          ),
          SizedBox(height: 15.0),
          Divider(color: Colors.black, thickness: 1,),
          SizedBox(height: 15.0),
          RaisedButton(
            color: Colors.redAccent,
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: Text(
              'Войти',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20.0,
              ),
            ),
            onPressed: () => Navigator.of(context)
                .pushReplacementNamed('login'),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    passwordConfirmController.dispose();
    super.dispose();
  }
}
