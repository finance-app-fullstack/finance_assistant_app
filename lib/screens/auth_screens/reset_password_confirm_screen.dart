import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../exceptions.dart';
import '../../repository.dart';
import '../../models/errors.dart';
import '../../utils.dart';

class ResetPasswordConfirmScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => ResetPasswordConfirmState();

}

class ResetPasswordConfirmState extends State<ResetPasswordConfirmScreen> {
  TextEditingController pinCodeController;
  TextEditingController newPasswordController;
  TextEditingController newPasswordConfirmController;
  Errors errors;
  bool loading;


  @override
  void initState() {
    pinCodeController = TextEditingController();
    newPasswordController = TextEditingController();
    newPasswordConfirmController = TextEditingController();
    errors = Errors();
    loading = false;
    super.initState();
  }

  Future changePassword() async {
    setState(() => loading = true);

    OperationsRepository apiRepo = OperationsRepository();
    try {
      await apiRepo.resetPassword(
          pinCodeController.text,
          newPasswordController.text,
          newPasswordConfirmController.text,
          ModalRoute.of(context).settings.arguments,
      );
      Navigator.of(context).pushReplacementNamed(
          'login',
          arguments: 'afterRestorePassword'
      );
    } on DataRepositoryException catch(e) {
      setState(() {
        loading = false;
        errors = e.errors;
      });
      if (errors.generalErrorsAsString.length > 0) {
        showErrorSnackBar(errors.generalErrorsAsString, context);
      }

    } catch (e) {
      setState(() {
        loading = false;
      });
      showErrorSnackBar('При подтверждении пароля произошла ошибка.', context);
    }

  }

  @override
  Widget build(BuildContext context) {
    return loading ?
    Center(child: CircularProgressIndicator()) :
    Form(
      child: ListView(
        padding: EdgeInsets.fromLTRB(24.0, 50.0, 24.0, 50.0),
        children: <Widget>[
          TextFormField(
            keyboardType: TextInputType.number,
            controller: pinCodeController,
            decoration: InputDecoration(
                hintText: 'Пин код',
                errorText: errors.fieldLevelAsString['pinCode']
            ),
          ),
          SizedBox(height: 30.0),
          TextFormField(
            obscureText: true,
            controller: newPasswordController,
            decoration: InputDecoration(
                hintText: 'Новый пароль',
                errorText: errors.fieldLevelAsString['password']
            ),
          ),
          SizedBox(height: 30.0),
          TextFormField(
            obscureText: true,
            controller: newPasswordConfirmController,
            decoration: InputDecoration(
                hintText: 'Подтверждение пароля',
                errorText: errors.fieldLevelAsString['passwordConfirm']
            ),
          ),
          SizedBox(height: 30.0),
          RaisedButton(
              color: Colors.lightBlueAccent,
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                  'Сменить пароль',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0
                  )
              ),
              onPressed: () => changePassword()
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    pinCodeController.dispose();
    newPasswordController.dispose();
    newPasswordConfirmController.dispose();
    super.dispose();
  }
}