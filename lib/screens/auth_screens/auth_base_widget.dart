import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'registration_screen.dart';
import 'login_screen.dart';
import 'reset_passord_screen.dart';
import 'reset_password_confirm_screen.dart';


class AuthScreen extends StatelessWidget {
  final String title;
  final String formId;

  const AuthScreen({Key key, this.title, this.formId}) : super(key: key);

  Widget getFormWidget(String formId){
    switch(formId){
      case 'login': return LoginScreen();
      case 'registration': return RegistrationScreen();
      case 'reset_password': return ResetPasswordScreen();
      case 'reset_password_confirm': return ResetPasswordConfirmScreen();
      default: {
        return LoginScreen();
      }
      break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(title)),
        body: getFormWidget(formId)
    );
  }
}
