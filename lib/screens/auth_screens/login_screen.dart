import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../exceptions.dart';
import '../../repository.dart';
import '../../models/errors.dart';
import '../../utils.dart';

class LoginScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => LoginScreenState();

}

class LoginScreenState extends State<LoginScreen> {
  TextEditingController emailController;
  TextEditingController passwordController;
  Errors errors;
  bool loading;

  @override
  void initState() {
    emailController = TextEditingController();
    passwordController = TextEditingController();
    errors = Errors();
    loading = false;
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_){
      if (ModalRoute.of(context).settings.arguments == 'afterRestorePassword'){
        showSuccessSnackBar('Пароль был успешно изменен.', context);
      }
    });
  }

  Future login() async {
    setState(() {
      loading = true;
    });

    OperationsRepository apiRepo = OperationsRepository();
    try {
      await apiRepo.login(emailController.text, passwordController.text);
      Navigator.of(context).popAndPushNamed('home');
    } on DataRepositoryException catch(e) {
      setState(() {
        loading = false;
        errors = e.errors;
      });
      if (errors.generalErrorsAsString.length > 0) {
        showErrorSnackBar(errors.generalErrorsAsString, context);
      }

    } catch (e) {
      setState(() {
        loading = false;
      });
      showErrorSnackBar('При авторизации произошла ошибка', context);
    }

  }


  @override
  Widget build(BuildContext context) {
    return loading ?
    Center(child: CircularProgressIndicator()) :
    Form(
      child: ListView(
        padding: EdgeInsets.fromLTRB(24.0, 50.0, 24.0, 50.0),
        children: <Widget>[
          TextFormField(
            keyboardType: TextInputType.emailAddress,
            controller: emailController,
            decoration: InputDecoration(
                hintText: 'Email',
                errorText: errors.fieldLevelAsString['email']
            ),
          ),
          SizedBox(height: 16.0),
          TextFormField(
            obscureText: true,
            controller: passwordController,
            decoration: InputDecoration(
                hintText: 'Пароль',
                errorText: errors.fieldLevelAsString['password']
            ),
          ),
          SizedBox(height: 30.0),
          RaisedButton(
            color: Colors.lightBlueAccent,
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: Text(
              'Войти',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20.0
              )
            ),
              onPressed: () => login()
          ),
          SizedBox(height: 15.0),
          Divider(color: Colors.black, thickness: 1,),
          SizedBox(height: 15.0),
          RaisedButton(
            color: Colors.redAccent,
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: Text(
              'Регистрация',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20.0,
              ),
            ),
            onPressed: () => Navigator.of(context)
                .pushReplacementNamed('registration'),
          ),
          SizedBox(height: 30.0),
          GestureDetector(
            onTap: () => Navigator.of(context)
                .pushNamed('reset_password'),
            child: Text('Забыли пароль?',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.lightBlue,
                fontSize: 18.0,
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }
}
