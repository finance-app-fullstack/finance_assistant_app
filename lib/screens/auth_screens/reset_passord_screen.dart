import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../exceptions.dart';
import '../../repository.dart';
import '../../models/errors.dart';
import '../../utils.dart';

class ResetPasswordScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => ResetPasswordScreenState();

}

class ResetPasswordScreenState extends State<ResetPasswordScreen> {
  TextEditingController emailController;
  Errors errors;
  bool loading;

  @override
  void initState() {
    emailController = TextEditingController();
    errors = Errors();
    loading = false;
    super.initState();
  }

  Future submit() async {
    setState(() => loading = true);

    OperationsRepository apiRepo = OperationsRepository();
    try {
      await apiRepo.resetPasswordGeneratePin(emailController.text);
      Navigator.of(context).pushReplacementNamed('reset_password_confirm',
          arguments: emailController.text);
    } on DataRepositoryException catch(e) {
      setState(() {
        loading = false;
        errors = e.errors;
      });
      if (errors.generalErrorsAsString.length > 0) {
        showErrorSnackBar(errors.generalErrorsAsString, context);
      }

    } catch (e) {
      setState(() => loading = false);
      showErrorSnackBar('При сбросе пароля произошла ошибка.', context);
    }

  }

  @override
  Widget build(BuildContext context) {
    return loading ?
    Center(child: CircularProgressIndicator()) :
    Form(
      child: ListView(
        padding: EdgeInsets.fromLTRB(24.0, 50.0, 24.0, 50.0),
        children: <Widget>[
          TextFormField(
            keyboardType: TextInputType.emailAddress,
            controller: emailController,
            decoration: InputDecoration(
                hintText: 'Email',
                errorText: errors.fieldLevelAsString['email']
            ),
          ),
          SizedBox(height: 30.0),
          RaisedButton(
              color: Colors.lightBlueAccent,
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                  'Сбросить пароль',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0
                  )
              ),
              onPressed: () => submit()
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    emailController.dispose();
    super.dispose();
  }
}