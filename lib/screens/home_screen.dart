import 'dart:developer';

import 'package:financeappprovider/screens/statistic_screen.dart';
import 'package:financeappprovider/view_models/categories_list_view_model.dart';
import 'package:financeappprovider/view_models/statistic_categories_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;

import '../widgets/operations_list/operations_filter_form.dart';
import '../repository.dart';
import 'category_form.dart';
import 'operation_form.dart';
import 'operations_screen.dart';
import 'categories_screen.dart';
import '../view_models/operations_list_view_model.dart';
import 'profile_screen.dart';


enum MenuTab { operations, categories, stats, profile }

class HomeScreen extends StatefulWidget {
  final OperationsRepository repository;

  const HomeScreen({Key key, this.repository}) : super(key: key);

  @override
  State<StatefulWidget> createState() => HomeScreenState();

}


class HomeScreenState extends State<HomeScreen> {
  MenuTab currentTab;
  List<Widget> actions;

  @override
  void initState() {
    currentTab = MenuTab.operations;

    actions = getActions(currentTab);

    super.initState();
  }

  IconData getTabIcon(MenuTab tab) {
    switch (tab) {
      case MenuTab.operations:
        return Icons.credit_card;
      case MenuTab.categories:
        return Icons.category;
      case MenuTab.stats:
        return Icons.assessment;
      case MenuTab.profile:
        return Icons.info;
      default:
        return Icons.credit_card;
    }
  }

  String get title {
    switch (currentTab) {
      case MenuTab.operations:
        return "Операции";
      case MenuTab.categories:
        return "Категории";
      case MenuTab.stats:
        return "Статистика";
      case MenuTab.profile:
        return "Профиль";
      default:
        return "Заголовок не определен";
    }
  }

  String getTabTitle(MenuTab tab) {
    switch (tab) {
      case MenuTab.operations:
        return "Операции";
      case MenuTab.categories:
        return "Категории";
      case MenuTab.stats:
        return "Статистика";
      case MenuTab.profile:
        return "Профиль";
      default:
        return "Заголовок не определен";
    }
  }

  Widget get tabContent {
    switch(currentTab){
      case MenuTab.operations:
        return OperationsList();
        break;
      case MenuTab.categories:
        return CategoriesList();
        break;
      case MenuTab.stats:
        return CategoriesStatisticScreen();
        break;
      case MenuTab.profile:
        return ProfileScreen(
            repository: widget.repository,
        );
        break;
      default:
        return OperationsList();
        break;
    }
  }

  Widget get floatingButton {
    switch(currentTab){
      case MenuTab.operations:
        return FloatingActionButton(
            onPressed: () async {
              bool result = await Navigator.push(context,
                MaterialPageRoute(
                    builder: (context) => OperationFormScreen(
                        repository: widget.repository
                    )
                ),
              );
              if (result != null && result) {
                Provider.of<OperationsListViewModel>(context, listen: false)
                    .loadOperations(initLoading: true);
                Provider.of<StatisticCategoriesViewModel>
                  (context, listen: false).loadStat();
              }
          },
          child: Icon(Icons.add),
          backgroundColor: Theme.of(context).accentColor,
        );
      case MenuTab.categories:
        return FloatingActionButton(
          onPressed: () async {
            bool result = await Navigator.push(context,
              MaterialPageRoute(
                  builder: (context) => CategoryFormScreen(
                      repository: widget.repository
                  )
              ),
            );
            if (result != null && result) {
              Provider.of<OperationsListViewModel>(context, listen: false)
                  .loadOperations(initLoading: true);
              Provider.of<CategoriesListViewModel>(context, listen: false)
                  .loadCategories();
              Provider.of<StatisticCategoriesViewModel>(context, listen: false)
                  .loadStat();
            }
          },
          child: Icon(Icons.add),
          backgroundColor: Theme.of(context).accentColor,
        );
      case MenuTab.stats:
        return FloatingActionButton(
          onPressed: () async {
            StatisticCategoriesViewModel vm = Provider
                .of<StatisticCategoriesViewModel>(context, listen: false);
            final List<DateTime> statisticRange = await
            DateRagePicker.showDatePicker(
                context: context,
                initialFirstDate: vm.start,
                initialLastDate: vm.end,
                firstDate: DateTime(1900),
                lastDate: DateTime.now()
            );
            if (statisticRange != null && statisticRange.length == 2) {
              vm.loadStat(
                  start: statisticRange[0],
                  end: statisticRange[1]
              );
            }
          },
          child: Icon(Icons.calendar_today_rounded),
          backgroundColor: Theme.of(context).accentColor,
        );
      default:
        return null;
    }
  }

  Future<Map> openSearchFormDialog(Map searchParams) async {
    return Navigator.of(context).push(MaterialPageRoute<Map>(
        builder: (BuildContext context) {
          return Scaffold(
              appBar: AppBar(
                title: Text('Фильтр'),
              ),
              body: OperationFilterForm(
                repository: widget.repository,
                searchData: searchParams,
              ),
          );
        },
        fullscreenDialog: true)
    );
  }

  List<Widget> getActions(MenuTab tab) {
    switch(tab){
      case MenuTab.operations:
        OperationsListViewModel modelView = Provider.of<OperationsListViewModel>
          (context, listen: false);
        log(modelView.isSearch.toString());
        return [
          IconButton(
              icon: Icon(Icons.search),
              onPressed:() async {
                Map result = await openSearchFormDialog(modelView.searchData);
                if (result != null) {
                  modelView.startSearch(
                      result['categories'],
                      result['dateRangeVariant'],
                      result['start'],
                      result['end']
                  );
                  setState(() {
                    actions.insert(
                      0,
                      Center(
                        child: GestureDetector(
                          onTap: () {
                            modelView.endSearch();
                            setState(() {
                              actions.removeAt(0);
                            });
                          },
                          child: Text('Сбросить фильтр'),
                        ),
                      )
                    );
                  });
                }
              }
          )
        ];
      case MenuTab.categories:
        return [];
      case MenuTab.stats:
        return [];
      case MenuTab.profile:
        return [];
      default:
        return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
          actions: actions,
        ),
        body: tabContent,
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: MenuTab.values.indexOf(currentTab),
          onTap: (index) => setState(() {
            currentTab = MenuTab.values[index];
            actions = getActions(MenuTab.values[index]);
          }),
          selectedItemColor: Colors.black,
          unselectedItemColor: Colors.black54,
          showUnselectedLabels: true,
          items: MenuTab.values.map((tab) {
            return BottomNavigationBarItem(
              icon: Icon(getTabIcon(tab)),
              label: getTabTitle(tab),
            );
          }).toList(),
        ),
      floatingActionButton: floatingButton
    );
  }
}