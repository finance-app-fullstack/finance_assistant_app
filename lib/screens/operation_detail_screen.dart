import 'package:financeappprovider/models/operation.dart';
import 'package:financeappprovider/widgets/operations_detail/operation_detail.dart';
import 'package:flutter/material.dart';

import '../repository.dart';

class OperationDetailScreen extends StatelessWidget {
  final OperationsRepository repository;
  final int operationId;

  const OperationDetailScreen({Key key,
    @required this.repository,
    @required this.operationId
  })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: repository.getDetailOperation(operationId),
        builder: (BuildContext context, AsyncSnapshot<Operation> snapshot){
          Widget content;

          if (snapshot.hasData) {
            content = OperationDetail(
                operation: snapshot.data,
                operationsRepository: repository,
            );
          }
          else if (snapshot.hasError) {
            content = Center(
                child: Text('При загрузке операции произошла ошибка :(')
            );
          } else {
            content = Center(
                child: CircularProgressIndicator()
            );
          }
          return Scaffold(
              appBar: AppBar(
                title: Text('Информация об операции'),
              ),
              body: content,
          );
        });
  }
}