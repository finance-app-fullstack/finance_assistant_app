import 'package:financeappprovider/view_models/statistic_categories_model.dart';
import 'package:financeappprovider/widgets/statistic/statistic_pie_charts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';


class CategoriesStatisticScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    WidgetsBinding.instance.addPostFrameCallback((_) {
      StatisticCategoriesViewModel vm = Provider
          .of<StatisticCategoriesViewModel>(context, listen: false);
      if (vm.categoriesStat == null){
        vm.loadStat();
      }
    });
    return Consumer<StatisticCategoriesViewModel>(
        builder: (context, categoriesStatModel, child) {
          return categoriesStatModel.loading ?
          Center(child: CircularProgressIndicator()) :
          CategoriesPieChart(categoriesStatModel);
        });
  }
}