import 'package:financeappprovider/models/operation.dart';
import 'package:flutter/material.dart';

import '../repository.dart';
import '../widgets/categories/category_detail.dart';

class CategoryDetailScreen extends StatelessWidget {
  final OperationsRepository repository;
  final int categoryId;

  const CategoryDetailScreen({Key key,
    @required this.repository,
    @required this.categoryId
  })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: repository.getDetailCategory(categoryId),
        builder: (BuildContext context,
            AsyncSnapshot<OperationCategoryFullInfo> snapshot){
          Widget content;

          if (snapshot.hasData) {
            content = CategoryDetail(
                category: snapshot.data,
                operationsRepository: repository,
            );
          }
          else if (snapshot.hasError) {
            content = Center(
                child: Text('При загрузке категории произошла ошибка :(')
            );
          } else {
            content = Center(
                child: CircularProgressIndicator()
            );
          }
          return Scaffold(
              appBar: AppBar(
                title: Text('Информация о категории'),
              ),
              body: content,
          );
        });
  }
}