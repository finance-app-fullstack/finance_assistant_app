import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import '../view_models/categories_list_view_model.dart';
import '../widgets/categories/categories_list_view.dart';



class CategoriesList extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    WidgetsBinding.instance.addPostFrameCallback((_) {
      CategoriesListViewModel vm = Provider.of<CategoriesListViewModel>
        (context, listen: false);
      if (vm.categories == null){
        vm.loadCategories();
      }
    });
    return Consumer<CategoriesListViewModel>(
        builder: (context, categoriesModel, child) {
          return categoriesModel.loading ?
            Center(child: CircularProgressIndicator()) :
            CategoriesListView(
              viewModel: categoriesModel,
              reloadCategories: categoriesModel.loadCategories,
            );
        });
    }
}