import 'dart:developer';

import 'package:financeappprovider/models/errors.dart';
import 'package:financeappprovider/models/operation.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

import '../exceptions.dart';
import '../repository.dart';
import '../utils.dart';

class OperationFormScreen extends StatefulWidget {
  final OperationsRepository repository;
  final Operation operation;

  const OperationFormScreen({Key key,
    @required this.repository,
    this.operation
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return OperationFormScreenState();
  }
}

class OperationFormScreenState extends State<OperationFormScreen> {
  DateFormat dateFormatter = DateFormat('d MMM y', 'ru_RU');
  String title;
  Errors errors;
  bool loading;
  bool createMode;
  List<OperationCategory> categories;
  int selectedCategory;
  DateTime createdDate;

  TextEditingController nameController;
  TextEditingController amountController;
  TextEditingController createdValueController;

  @override
  void initState() {
    loading = true;
    errors = Errors();

    nameController = TextEditingController();
    amountController = TextEditingController();
    createdValueController = TextEditingController();

    categories = [];

    if (widget.operation != null) {
      nameController.text = widget.operation.name;
      amountController.text = widget.operation.amount.toString();
      selectedCategory = widget.operation.categoryId;
      createdDate = widget.operation.created;
      createdValueController.text = dateFormatter.format(createdDate);
      title = 'Обновление операции';
      createMode = false;
    } else {
      title = 'Новая операция';
      createMode = true;
      createdDate = DateTime.now();
    }

    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_){
      widget.repository.getCategories()
          .then((value) {
            setState(() {
              value.forEach((element) {
                categories.add(element);
              });
              loading = false;
            });
          })
          .catchError((onError) {
            setState(() {
              loading = false;
            });
          });
    });
  }

  void updateOperation() async {
    setState(() {
      loading = true;
    });
    try {
      await widget.repository.updateOperation(
        widget.operation.id,
        nameController.text,
        selectedCategory,
        int.parse(amountController.text),
        createdDate
      );
      Navigator.of(context).pop();
      Navigator.of(context).pop(true);
    } on DataRepositoryException catch(e) {
      setState(() {
        loading = false;
        errors = e.errors;
      });
      if (errors.generalErrorsAsString.length > 0) {
        showErrorSnackBar(errors.generalErrorsAsString, context);
      }

    } catch (e) {
      setState(() {
        loading = false;
      });
      showErrorSnackBar('При сохранении операции произошла ошибка.', context);
    }
  }

  void createOperation() async {
    setState(() {
      loading = true;
    });
    try {
      await widget.repository.createOperation(
          nameController.text,
          selectedCategory,
          int.parse(amountController.text),
          createdDate
      );
      Navigator.of(context).pop(true);
    } on DataRepositoryException catch(e) {
      setState(() {
        loading = false;
        errors = e.errors;
      });
      if (errors.generalErrorsAsString.length > 0) {
        showErrorSnackBar(errors.generalErrorsAsString, context);
      }

    } catch (e) {
      setState(() {
        loading = false;
      });
      showErrorSnackBar('При сохранении операции произошла ошибка.', context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: loading ?
      Center(child: CircularProgressIndicator()) :
      ListView(
          padding: EdgeInsets.fromLTRB(24.0, 15.0, 24.0, 50.0),
          children: <Widget>[
            TextField(
              keyboardType: TextInputType.text,
              controller: nameController,
              decoration: InputDecoration(
                  hintText: 'Название',
                  errorText: errors.fieldLevelAsString['name']
              ),
            ),
            SizedBox(height: 16.0),
            TextField(
              keyboardType: TextInputType.number,
              controller: amountController,
              decoration: InputDecoration(
                  hintText: 'Сумма',
                  errorText: errors.fieldLevelAsString['amount']
              ),
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.digitsOnly
              ],
            ),
            SizedBox(height: 16.0),
            DropdownButtonFormField(
              items: categories.map((OperationCategory category) {
                return DropdownMenuItem(
                    value: category.id,
                    child: Text(category.name)
                );
              }).toList(),
              decoration: InputDecoration(
                  hintText: 'Категория',
                  errorText: errors.fieldLevelAsString['category']
              ),
              onChanged: (value) => {setState(() => selectedCategory = value)},
              value: selectedCategory,
            ),
            SizedBox(height: 16.0),
            TextField(
              readOnly: true,
              controller: createdValueController,
              decoration: InputDecoration(
                  hintText: 'Дата',
                  errorText: errors.fieldLevelAsString['date']
              ),
              onTap: () async {
                DateTime result = await showDatePicker(
                  context: context,
                  initialDate: createdDate,
                  firstDate: DateTime(1969),
                  lastDate: DateTime(3000)
                );
                log(result.toString());
                if (result != null) {
                  setState(() {
                    createdDate = result;
                  });
                  createdValueController.text = dateFormatter.format(result);
                }
              }
            ),
            SizedBox(height: 30.0),
            RaisedButton(
                color: Colors.lightBlueAccent,
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: Text(
                    'Сохранить',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20.0
                    )
                ),
                onPressed: () => createMode ? createOperation()
                    : updateOperation()
            ),
            SizedBox(height: 15.0),
          ],
      ),
    );
  }

  @override
  void dispose() {
    nameController.dispose();
    amountController.dispose();
    createdValueController.dispose();
    super.dispose();
  }

}
