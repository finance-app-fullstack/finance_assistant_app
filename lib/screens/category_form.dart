import 'package:financeappprovider/models/errors.dart';
import 'package:financeappprovider/models/operation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

import '../exceptions.dart';
import '../repository.dart';
import '../utils.dart';

class CategoryFormScreen extends StatefulWidget {
  final OperationsRepository repository;
  final OperationCategory category;

  const CategoryFormScreen({Key key,
    @required this.repository,
    this.category
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return CategoryFormScreenState();
  }
}

class CategoryFormScreenState extends State<CategoryFormScreen> {
  String title;
  Errors errors;
  bool loading;
  bool createMode;

  TextEditingController nameController;

  @override
  void initState() {
    loading = false;
    errors = Errors();

    nameController = TextEditingController();

    if (widget.category != null) {
      nameController.text = widget.category.name;
      title = 'Обновление категории';
      createMode = false;
    } else {
      title = 'Новая категория';
      createMode = true;
    }

    super.initState();
  }

  void updateCategory() async {
    setState(() {
      loading = true;
    });
    try {
      await widget.repository.updateCategory(
        widget.category.id,
        nameController.text,
      );
      Navigator.of(context).pop();
      Navigator.of(context).pop(true);
    } on DataRepositoryException catch(e) {
      setState(() {
        loading = false;
        errors = e.errors;
      });
      if (errors.generalErrorsAsString.length > 0) {
        showErrorSnackBar(errors.generalErrorsAsString, context);
      }

    } catch (e) {
      setState(() {
        loading = false;
      });
      showErrorSnackBar('При сохранении операции произошла ошибка.', context);
    }
  }

  void createCategory() async {
    setState(() {
      loading = true;
    });
    try {
      await widget.repository.createCategory(nameController.text);
      Navigator.of(context).pop(true);
    } on DataRepositoryException catch(e) {
      setState(() {
        loading = false;
        errors = e.errors;
      });
      if (errors.generalErrorsAsString.length > 0) {
        showErrorSnackBar(errors.generalErrorsAsString, context);
      }

    } catch (e) {
      setState(() {
        loading = false;
      });
      showErrorSnackBar('При сохранении категории произошла ошибка.', context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: loading ?
      Center(child: CircularProgressIndicator()) :
      ListView(
          padding: EdgeInsets.fromLTRB(24.0, 15.0, 24.0, 50.0),
          children: <Widget>[
            TextField(
              keyboardType: TextInputType.text,
              controller: nameController,
              decoration: InputDecoration(
                  hintText: 'Название',
                  errorText: errors.fieldLevelAsString['name']
              ),
            ),
            SizedBox(height: 30.0),
            RaisedButton(
                color: Colors.lightBlueAccent,
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: Text(
                    'Сохранить',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20.0
                    )
                ),
                onPressed: () => createMode ? createCategory()
                    : updateCategory()
            ),
            SizedBox(height: 15.0),
          ],
      ),
    );
  }

  @override
  void dispose() {
    nameController.dispose();
    super.dispose();
  }

}
