import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import '../view_models/operations_list_view_model.dart';
import '../widgets/operations_list/operations_list_view.dart';


class OperationsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<OperationsListViewModel>(
        builder: (context, operationsModel, child) {
          return operationsModel.loading ?
            Center(child: CircularProgressIndicator()) :
            OperationsListView(viewModel: operationsModel,);
        });
    }
}