import 'dart:developer' as developer;
import 'dart:math';

import 'backend_api_client.dart';
import 'models/errors.dart';
import 'exceptions.dart';
import 'models/operation.dart';
import 'utils.dart';

class OperationsRepository {
  final Duration delay;
  final BackendHttpClient httpClient = BackendHttpClient();


  OperationsRepository({this.delay: const Duration(milliseconds: 3000)});


  Future<List<Operation>> getOperations(
      num page,
      {
        num itemPerPage: 20,
        List<int> categories,
        DateTime startDate,
        DateTime endDate
      }) async {
    return Future.delayed(delay, () {
      num maxPages = 10;

      if (categories != null && startDate != null && endDate != null){
        maxPages = 2;
      }

      if (page >= maxPages){
        return [];
      }
      num start = ((page - 1) * itemPerPage) + 1;
      num end = page * itemPerPage;

      Map<num, Map> operationsInfo = {
        0: {'name': 'Корм коту', 'category': 'Развлечения', 'amount': 800},
        1: {'name': 'Поход в магазин', 'category': 'Продукты', 'amount': 1000},
        2: {'name': 'Заправка', 'category': 'Авто', 'amount': 2500},
        3: {'name': 'Мойка', 'category': 'Авто', 'amount': 800},
        4: {'name': 'ТО', 'category': 'Авто', 'amount': 10000},
        5: {'name': 'Таблетки от кашля', 'category': 'Аптека', 'amount': 300},
        6: {'name': 'Витамины', 'category': 'Аптека', 'amount': 200},
        7: {'name': 'Оплата коммунальных', 'category': 'Квартира',
          'amount': 8000},
        8: {'name': 'Новый пылесос', 'category': 'Квартира', 'amount': 12000},
        9: {'name': 'Поход в кафе', 'category': 'Развлечения', 'amount': 1500},
        10: {'name': 'Подписка музыку', 'category': 'Развлечения',
          'amount': 600},
      };

      List<Operation> operations = [];
      for(var i = start ; i <= end; i++ ) {
        Random random = new Random();
        num key = random.nextInt(10);
        Map operation = operationsInfo[key];
        operations.add(
            Operation(
                i,
                operation['name'],
                operation['amount'],
                operation['category'],
                DateTime.now().add(Duration(hours: i)).toIso8601String()
            )
        );
      }

      return operations;

    });
  }

  Future<bool> createOperation(String name, int categoryId, int amount,
      DateTime created) async {
    return Future.delayed(delay, () => true);
  }

  Future<bool> updateOperation(int id, String name, int categoryId, int amount,
      DateTime created)
  async {
    return Future.delayed(delay, () => true);
  }

  Future<bool> deleteOperation(int id) async {
    return Future.delayed(delay, () => true);
  }

  Future<Operation> getDetailOperation(int id) async {
    return Future.delayed(delay, () {
      return Operation(id, 'Мойка авто', 500, 'Авто',
          DateTime.now().toIso8601String(), categoryId: 2);
    });
  }

  Future<List<OperationCategory>> getCategories() async {
    return Future.delayed(delay, () {
      List<OperationCategory> categories = [];
      categories.add(OperationCategory(0, 'Развлечения'));
      categories.add(OperationCategory(1, 'Продукты'));
      categories.add(OperationCategory(2, 'Авто'));
      categories.add(OperationCategory(3, 'Аптека'));
      categories.add(OperationCategory(4, 'Квартира'));
      return categories;

    });
  }

  Future<OperationCategoryFullInfo> getDetailCategory(int id) async {
    return Future.delayed(delay, () {
      return OperationCategoryFullInfo(id, 'Авто', 210);
    });
  }

  Future<bool> updateCategory(int categoryId, String name) async {
    return Future.delayed(delay, () => true);
  }

  Future<bool> deleteCategory(int categoryId) async {
    return Future.delayed(delay, () => true);
  }

  Future<bool> createCategory(String name) async {
    return Future.delayed(delay, () => true);
  }

  Future<List<DateTime>> getOperationsDateRange() async {
    return Future.delayed(delay, () {
      return [DateTime.now().subtract(Duration(days: 30)), DateTime.now()];

    });
  }

  Future login(String email, String password) async {
    Map<String, String> body = {
      'email': email,
      'password': password
    };
    Map<String, String> responseData = await httpClient.login(body);
    await setAuthData(email: email, apiKey: responseData['token']);
  }

  Future logout() async {
    await removeAuthData();
    await httpClient.logout();
  }

  Future<String> registration(String email, String password,
      String passwordConfirm) async {
    Map<String, String> body = {
      'email': email,
      'password': password,
      'password_confirm': passwordConfirm
    };
    Map<String, String> responseData = await httpClient.registrationUser(body);
    await setAuthData(apiKey: responseData['token'], email: email);
    return responseData['token'];
  }

  Future changePassword(String password, String confirmPassword) async {
    return Future.delayed(delay, () async {
      if (password != confirmPassword) {
        throw DataRepositoryException(
            Errors(fieldLevelErrors: {
              'password': 'Пaроли не совпадают.',
              'passwordConfirm': 'Пaроли не совпадают.'
            })
        );
      }
    });
  }

  Future resetPasswordGeneratePin(String email) async {
    Map<String, String> data = {'email': email};
    await httpClient.resetPasswordCreatePin(data);
  }

  Future resetPassword(String pin, String password,
      String passwordConfirm, String email) async {
    Map<String, String> data = {
      'pin': pin.toString(),
      'password': password,
      'password_confirm': passwordConfirm,
      'email': email,
    };
    await httpClient.resetPassword(data);
  }

  Future<List<OperationCategoryStatInfo>> getStatisticInfo(DateTime start,
      DateTime end)
  {
    return Future.delayed(delay, () async {
      Random random = new Random();
      Map sumValues = {
        0: 1000,
        1: 5500,
        2: 10000,
        3: 8000,
      };
      List<String> names = [
        'Авто',
        'Коммунальные',
        'Питание',
        'Здоровье',
      ];
      
      List<OperationCategoryStatInfo> result = [];

      names.forEach((categoryName) {
        num randomSumKey = random.nextInt(4);
        developer.log(sumValues[randomSumKey].toString());
        developer.log(categoryName);
        result.add(
          OperationCategoryStatInfo(categoryName, sumValues[randomSumKey])
        );
      });
      return result;
    });
  }
}