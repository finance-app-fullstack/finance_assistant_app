import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';


Future<Map<String, String>> getAuthData() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  Map<String, String> userData = {};

  userData['apiKey'] = prefs.getString('apiKey');
  userData['email'] = prefs.getString('email');
  return userData;
}

Future setAuthData({String apiKey, String email}) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (apiKey != null) {
    prefs.setString('apiKey', apiKey);
  }
  if (email != null) {
    prefs.setString('email', email);
  }

}

Future removeAuthData() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.remove('apiKey');
  prefs.remove('email');
}

void showErrorSnackBar(String error, BuildContext context){
  Scaffold.of(context).hideCurrentSnackBar();
  SnackBar snackBar = SnackBar(
    content: Text(error),
    duration: Duration(seconds: 10),
    action: SnackBarAction(
      label: 'Закрыть',
      textColor: Colors.redAccent,
      onPressed: () {
        Scaffold.of(context).hideCurrentSnackBar();
      },
    ),
  );
  Scaffold.of(context).showSnackBar(snackBar);
}

void showSuccessSnackBar(String message, BuildContext context){
  Scaffold.of(context).hideCurrentSnackBar();
  SnackBar snackBar = SnackBar(
    content: Text(message),
    duration: Duration(seconds: 10),
    action: SnackBarAction(
      label: 'Закрыть',
      textColor: Colors.blueAccent,
      onPressed: () {
        Scaffold.of(context).hideCurrentSnackBar();
      },
    ),
  );
  Scaffold.of(context).showSnackBar(snackBar);
}

NumberFormat getAmountFormatter() => NumberFormat.currency(
    locale: 'ru_RU', symbol: '₽', decimalDigits: 0);

